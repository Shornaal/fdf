/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 21:44:31 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/01 21:44:33 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>

# define BUFF_SIZE 128

typedef struct			s_fd_list
{
	int					fd;
	char				*buffer;
	int					lline;
	struct s_fd_list	*next;
}						t_fd_list;

int						get_next_line(int const fd, char **line);
#endif
