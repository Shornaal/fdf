/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/18 21:53:20 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/01 20:21:36 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FDF_H
# define FT_FDF_H

#include <mlx.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <libft.h>
#include <get_next_line.h>

#define WINDOW_TITLE "Fils de FAF."
#define WINDOW_H 1280
#define WINDOW_W 1680
#define RED 0xB21212
#define YELLOW 0xFFFC19
#define RED2 0xFF0000
#define BLUE 0x0971B2
#define MOD_ISO 2
#define MOD_PAR 4
#define INT_MAX 0x7FFF/0x7FFFFFFF

typedef struct		s_point
{
	int				cx;
	int				cy;
	int				cz;
	int				dx;
	int				dy;
	int				color;
}					t_point;

typedef struct		s_fdf
{
	void			*mlxhandler;
	void			*mlxwin;
	t_point			**map;
	int				padding;
	int				padding_h;
	int				mode;
	int				map_h;
	int				map_w;
	int				max_z;
}					t_fdf;

typedef struct		s_bresenham
{
	int				delta_x;
	int				delta_y;
	int				err;
	int				sign_x;
	int				sign_y;
}					t_bresen;

int					fdf_init(t_fdf *fdf, char *path);
void				fdf_free(t_fdf *fdf);
t_point				**init_map(t_fdf *fdf, char *path);
t_list				*getmap(t_list *map, char *path);
int					draw(t_fdf *fdf);
void				draw_line(t_fdf *fdf, t_point *a, t_point *b);
void				calculate_coord(t_fdf *fdf);
void				calculate_iso(t_fdf *fdf, t_point *point);
void				erase(t_fdf *fdf);
void				zoom(t_fdf *fdf, int zoom);
int					expose_hook(t_fdf *fdf);
#endif
