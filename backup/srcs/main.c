/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/18 21:52:26 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/27 22:45:09 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

int		expose_hook(t_fdf *fdf)
{
	calculate_coord(fdf);
	draw(fdf);
	return (0);
}

int		key_hook(int keycode, t_fdf *fdf)
{
	if (keycode == 65307)
		fdf_free(fdf);
	else if (keycode == 65451 || keycode == 65453)
		zoom(fdf, keycode);
	else
		ft_putnbr(keycode);
	return (0);
}

int		main(int argc, char **argv)
{
	t_fdf	fdf;
	
	if (argc == 2)
	{
		fdf_init(&fdf, argv[1]);
		mlx_expose_hook(fdf.mlxwin, expose_hook, &fdf);
		mlx_key_hook(fdf.mlxwin, key_hook, &fdf);
		mlx_loop(fdf.mlxhandler);
	}
	return (0);
}
