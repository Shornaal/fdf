/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/20 18:28:35 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/27 22:35:21 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

static t_point	ft_create_point(int z, int x, int y)
{
	t_point		new_point;

	new_point.cx = x;
	new_point.cy = y;
	new_point.cz = z;
	new_point.dx = 0;
	new_point.dy = 0;
	new_point.color = 0xFFFFFF;
	return (new_point);
}

static t_point		*set_line(t_fdf *fdf, char *cline, int y)
{
	char	**nb;
	int		i;
	int		count;
	t_point	*line;

	count = 0;
	i = -1;
	nb = ft_strsplit(cline, ' ');
	while (nb[++i] != '\0')
		count++;
	fdf->map_w = (count > fdf->map_w) ? count : fdf->map_w;
	i = -1;
	if (!(line = (t_point *)ft_memalloc(sizeof(t_point) * count + 1)))
		return (NULL);
	while (nb[++i])
	{
		fdf->max_z = ((ft_atoi(nb[i]) > fdf->max_z) ? ft_atoi(nb[i]) : fdf->max_z);
		line[i] = ft_create_point(ft_atoi(nb[i]), i, y);
	}
	return (line);
}

t_point			**init_map(t_fdf *fdf, char *path)
{
	t_list	*map;
	int		length;
	int		i;

	i = 0;
	if (!(map = getmap(map, path)))
		return (NULL);
	length = ft_lstlen(&map);
	if (!(fdf->map = (t_point **)ft_memalloc(sizeof(t_point *) * length + 1)))
		return (NULL);
	fdf->map_h = (length > fdf->map_h) ? length : fdf->map_h;
	while (map)
	{
		fdf->map[i] = set_line(fdf, (char *)(map->content), i);
		map = map->next;
		i++;
	}
	fdf->map[i + 1] = NULL;
	return (fdf->map);
}

t_list			*getmap(t_list *map, char *path)
{
	char	*line;
	int		fd;
	int		ret;

	fd = open(path, O_RDONLY);
	if (fd == -1)
	{
		ft_putendl_fd("Error.", 2);
		exit(0);
		return (NULL);
	}
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		if (!map) 
			map = ft_lstnew(line, (sizeof(char) * ft_strlen(line) + 1));
		else
			ft_lstpushback(&map, ft_lstnew(line,
						(sizeof(char) * ft_strlen(line) + 1)));
	}
	close (fd);
	if (ret < 0)
	{
		ft_putstr_fd("Error", 2);
		exit(0);
	}
	return (map);
}
