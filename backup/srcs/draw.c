/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/26 20:28:19 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/27 22:59:13 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

int		draw(t_fdf *fdf)
{
	int		i;
	int		j;

	i = -1;
	while (++i < fdf->map_h)
	{
		j = -1;
		while (++j < fdf->map_w)
		{
			if (j < fdf->map_w - 1)
				draw_line(fdf, &(fdf->map[i][j]), &(fdf->map[i][j + 1]));
			if (i < fdf->map_h - 1)
				draw_line(fdf, &(fdf->map[i][j]), &(fdf->map[i + 1][j]));
		}
	}
	return (0);
}
