/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/26 18:07:10 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/01 20:20:02 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

int		fdf_init(t_fdf *fdf, char *path)
{
	if (!(fdf->mlxhandler = mlx_init()))
	{
		ft_putendl_fd("Error", 2);
		exit(0);
		return (-1);
	}
	fdf->mlxwin = mlx_new_window(fdf->mlxhandler, WINDOW_W, WINDOW_H, WINDOW_TITLE);	
	fdf->padding = 15;
	fdf->padding_h = 5;
	fdf->map_h = 0;
	fdf->map_w = 0;
	if (!(init_map(fdf, path)))
		return (-1);
	return (0);
}

void	fdf_free(t_fdf *fdf)
{
	int		i;

	i = -1;
	while (++i < fdf->map_h)
		free(fdf->map[i]);
	free(fdf->map);
	mlx_destroy_window(fdf->mlxhandler, fdf->mlxwin);
	free(fdf->mlxhandler);
	exit(0);
}
