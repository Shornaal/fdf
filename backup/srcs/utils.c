/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/27 22:23:55 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/27 22:43:14 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

void	erase(t_fdf *fdf)
{
	mlx_clear_window(fdf->mlxhandler, fdf->mlxwin);
	expose_hook(fdf);
}
