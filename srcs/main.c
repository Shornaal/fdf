/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/18 21:52:26 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/15 22:33:31 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

int		expose_hook(t_fdf *fdf)
{
	calculate_coord(fdf);
	draw(fdf);
	return (0);
}

int		key_hook(int keycode, t_fdf *fdf)
{
	if (keycode == 65307)
		fdf_free(fdf);
	else if (keycode == 65451 || keycode == 65453)
		zoom(fdf, keycode);
	else if (keycode == 109)
	{
		fdf->mode = (fdf->mode) ? 0 : 1;
		erase(fdf);
	}
	else if (keycode == 105)
		invert_z(fdf);
	else if (keycode == 111)
		input_z(fdf, 0);
	else if (keycode == 112)
		input_z(fdf, 1);
	else if (keycode == 65361 || keycode == 65363 || keycode == 65362 ||
			keycode == 65364)
		move(fdf, keycode);
	return (0);
}

int		main(int argc, char **argv)
{
	t_fdf	fdf;

	if (argc == 2)
	{
		if ((fdf_init(&fdf, argv[1])))
		{
			ft_putendl_fd("Error. \\o/", 2);
			return (-1);
		}
		mlx_expose_hook(fdf.mlxwin, expose_hook, &fdf);
		mlx_key_hook(fdf.mlxwin, key_hook, &fdf);
		mlx_loop(fdf.mlxhandler);
	}
	return (0);
}
