/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iso.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/27 18:47:40 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/15 22:17:38 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

void	calculate_coord(t_fdf *fdf)
{
	int		x;
	int		y;

	y = -1;
	while (++y < fdf->map_h)
	{
		x = -1;
		while (++x < fdf->map_w)
			calculate_iso(fdf, &(fdf->map[y][x]));
	}
}

void	calculate_iso(t_fdf *fdf, t_point *point)
{
	int		tx;
	int		ty;
	int		tz;

	tx = point->cx * fdf->padding;
	ty = point->cy * fdf->padding;
	tz = point->cz * fdf->padding_h;
	if (!fdf->mode)
	{
		point->dx = tx - ty + WINDOW_W * 0.5;
		point->dy = -tz + tx * 0.5 + ty * 0.5 + WINDOW_H * 0.3;
	}
	else
	{
		point->dx = tx - tz + WINDOW_W * 0.5;
		point->dy = ty + -1 * 0.5 * tz + WINDOW_H * 0.3;
	}
}
