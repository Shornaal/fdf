/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/27 22:23:55 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/15 22:39:19 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

void	move(t_fdf *fdf, int keycode)
{
	int		i;
	int		j;

	i = -1;
	while (++i < fdf->map_h)
	{
		j = -1;
		while (++j < fdf->line_len[i])
		{
			if (keycode == 65361 || keycode == 65363)
				fdf->map[i][j].cx += (keycode == 65361) ? -7 : 7;
			else if (keycode == 65362 || keycode == 65364)
				fdf->map[i][j].cy += (keycode == 65362) ? -7 : 7;
		}
	}
	erase(fdf);
}

void	invert_z(t_fdf *fdf)
{
	int		i;
	int		j;

	i = -1;
	while (++i < fdf->map_h)
	{
		j = -1;
		while (++j < fdf->line_len[i])
			fdf->map[i][j].cz *= -1;
	}
	erase(fdf);
}

void	input_z(t_fdf *fdf, int mode)
{
	int		i;
	int		j;

	i = -1;
	while (++i < fdf->map_h)
	{
		j = -1;
		while (++j < fdf->line_len[i])
			if (((fdf->map[i][j].cz + 5) < 2147483647) &&
				((fdf->map[i][j].cz - 5) > -2147483647) &&
					fdf->map[i][j].cz != 0)
				fdf->map[i][j].cz += (mode) ? 3 : -3;
	}
	erase(fdf);
}

void	erase(t_fdf *fdf)
{
	mlx_clear_window(fdf->mlxhandler, fdf->mlxwin);
	expose_hook(fdf);
}
