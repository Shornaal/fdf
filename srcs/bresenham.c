/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bresenham.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/26 21:05:04 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/15 22:38:20 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

static inline int	ft_abs(int value)
{
	if (value < 0)
		value *= -1;
	return (value);
}

static inline int	color(int za, int zb, int max)
{
	if (((za + zb) / 2) > (max * 0.8))
		return (RED);
	else if (((za + zb) / 2) == 0)
		return (BLUE);
	else if (((za + zb) / 2) < 0)
		return (RED2);
	else
		return (YELLOW);
}

static t_bresen		*build_bres(t_point *a, t_point *b, int max)
{
	t_bresen	*bres;

	if (!(bres = (t_bresen *)ft_memalloc(sizeof(t_bresen))))
		return (NULL);
	bres->delta_x = ft_abs(b->dx - a->dx);
	bres->sign_x = (a->dx < b->dx) ? 1 : -1;
	bres->delta_y = ft_abs(b->dy - a->dy);
	bres->sign_y = (a->dy < b->dy) ? 1 : -1;
	bres->err = ((bres->delta_x > bres->delta_y) ? bres->delta_x :
			-(bres->delta_y)) / 2;
	a->color = color(a->cz, b->cz, max);
	return (bres);
}

void				draw_line(t_fdf *fdf, t_point *a, t_point *b)
{
	int			x;
	int			y;
	t_bresen	*bres;
	int			e2;

	bres = build_bres(a, b, (fdf->max_z * fdf->padding_h));
	x = a->dx;
	y = a->dy;
	while (1)
	{
		if ((x < WINDOW_W && x > 0) && (y < WINDOW_H && y > 0))
			mlx_pixel_put(fdf->mlxhandler, fdf->mlxwin, x, y, a->color);
		if (x == b->dx && y == b->dy)
			break ;
		if (e2 = bres->err, e2 > -(bres->delta_x))
		{
			bres->err -= bres->delta_y;
			x += bres->sign_x;
		}
		if (e2 < bres->delta_y)
		{
			bres->err += bres->delta_x;
			y += bres->sign_y;
		}
	}
}
