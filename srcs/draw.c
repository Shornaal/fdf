/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/26 20:28:19 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/15 20:22:44 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

int		draw(t_fdf *fdf)
{
	int		i;
	int		j;

	i = 0;
	while (i < fdf->map_h)
	{
		j = 0;
		while (j < fdf->line_len[i])
		{
			if (j + 1 < (fdf->line_len[i]) && (i < fdf->map_h))
				draw_line(fdf, &(fdf->map[i][j]), &(fdf->map[i][j + 1]));
			if ((i + 1 < fdf->map_h) && (j < fdf->line_len[i + 1]))
				draw_line(fdf, &(fdf->map[i][j]), &(fdf->map[i + 1][j]));
			j++;
		}
		i++;
	}
	return (0);
}
