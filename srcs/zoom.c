/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zoom.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/27 22:27:32 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/27 23:11:42 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

void	zoom(t_fdf *fdf, int key)
{
	if (key == 65451)
		fdf->padding += 5;
	else if (key == 65453)
		fdf->padding -= 5;
	if (fdf->padding < 5)
		fdf->padding = 5;
	if (fdf->padding > 2147483640)
		fdf->padding = 2147483640;
	erase(fdf);
}
