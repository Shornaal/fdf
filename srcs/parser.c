/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/20 18:28:35 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/15 22:25:11 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

static void		clean_line(char *cline)
{
	int		i;

	i = 0;
	while (cline[i])
	{
		if (!(ft_isdigit(cline[i])) && cline[i] != '-')
			ft_creplace(cline, cline[i], ' ');
		i++;
	}
}

static t_point	ft_create_point(t_fdf *fdf, int z, int x, int y)
{
	t_point		new_point;

	new_point.cx = x + fdf->move_lr;
	new_point.cy = y + fdf->move_ud;
	new_point.cz = z;
	new_point.dx = 0;
	new_point.dy = 0;
	new_point.color = 0xFFFFFF;
	return (new_point);
}

static t_point	*set_line(t_fdf *fdf, char *cline, int y)
{
	char	**nb;
	int		i;
	int		count;
	t_point	*line;

	count = 0;
	i = -1;
	if (!(nb = ft_strsplit(cline, ' ')))
		return (NULL);
	while (nb[++i] != NULL)
		count++;
	fdf->line_len[y] = count;
	fdf->map_w = (count > fdf->map_w) ? count : fdf->map_w;
	i = -1;
	if (!(line = (t_point *)ft_memalloc(sizeof(t_point) * (count + 2))))
		return (NULL);
	while (nb[++i])
	{
		fdf->max_z = ((ft_atoi(nb[i]) > fdf->max_z) ?
			ft_atoi(nb[i]) : fdf->max_z);
		line[i] = ft_create_point(fdf, ft_atoi(nb[i]), i, y);
	}
	return (line);
}

t_point			**init_map(t_fdf *fdf, char *path)
{
	t_list	*map;
	int		length;
	int		i;

	map = NULL;
	i = 0;
	if (!(map = getmap(map, path)))
		return (NULL);
	length = ft_lstlen(&map);
	if (!(fdf->line_len = (int *)ft_memalloc(sizeof(int) * (length + 3))))
		return (NULL);
	if (!(fdf->map = (t_point **)ft_memalloc(sizeof(t_point *) * (length + 2))))
		return (NULL);
	fdf->map_h = (length > fdf->map_h) ? length : fdf->map_h;
	while (map)
	{
		fdf->map[i] = set_line(fdf, (char *)(map->content), i);
		map = map->next;
		i++;
	}
	fdf->map[i + 1] = NULL;
	return (fdf->map);
}

t_list			*getmap(t_list *map, char *path)
{
	char	*line;
	int		fd;
	int		ret;

	fd = open(path, O_RDONLY);
	if (fd < 0)
		return (NULL);
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		clean_line(line);
		line = ft_strtrim(line);
		if (line[0] != '\0')
		{
			if (!map)
				map = ft_lstnew(line, sizeof(char) * (ft_strlen(line) + 1));
			else
				ft_lstpushback(&map, ft_lstnew(line, sizeof(char) *
							(ft_strlen(line)) + 1));
		}
	}
	close (fd);
	if (ret < 0)
		return (NULL);
	return (map);
}
