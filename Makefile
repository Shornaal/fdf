# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/12/07 01:27:33 by tiboitel          #+#    #+#              #
#    Updated: 2015/03/15 18:36:20 by tiboitel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME 		 = 	fdf
SRCS 		 = 	main.c \
				parser.c \
				fdf.c \
				draw.c \
				bresenham.c \
				iso.c \
				get_next_line.c \
				utils.c \
				zoom.c \
				ft_len_untill.c
INCLUDES	 =	./includes
SRCSPATH 	 =	./srcs/
LIBFTPATH 	 =  ./libft/
LIBFTINC 	 =  ./libft/includes

CC			 = gcc
CFLAGS		 = -Wall -Werror -Wextra -Ofast
INCLUDES_O	 = -I $(LIBFTINC) -I $(INCLUDES) 
INCLUDES_C	 = -L $(LIBFTPATH) -lft -L /usr/X11/lib -lmlx -lXext -lX11


SRC			 = $(addprefix $(SRCSPATH), $(SRCS))
OBJS		 = $(SRC:.c=.o)

all:		$(NAME)

$(NAME):	$(OBJS)
			$(CC) -o $(NAME) $(OBJS) $(CFLAGS) $(INCLUDES_C) -g3

%.o: %.c libft/libft.a
			$(CC) -o $@ $(CFLAGS) $(INCLUDES_O) -c $<

libft/libft.a:
			make -C $(LIBFTPATH)

clean:
			make -C $(LIBFTPATH) clean
			rm -rf $(OBJS)

fclean: 	clean
			make -C $(LIBFTPATH) fclean
			rm -rf $(NAME)

re: fclean all

.PHONY: clean fclean re
			
